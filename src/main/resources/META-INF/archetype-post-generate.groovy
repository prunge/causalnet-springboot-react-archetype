import groovy.io.FileType
import org.apache.maven.shared.utils.io.FileUtils
import java.nio.file.Files

@Grab(group='com.fasterxml.jackson.core', module='jackson-databind', version='2.4.3')
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.core.type.*

class Project {
    File projectDirectory

    void run(String... cmd) {
        def process = new ProcessBuilder(cmd).directory(projectDirectory).inheritIO().start()
        process.waitForProcessOutput((Appendable)System.out, System.err)
        if (process.exitValue() != 0) {
            throw new Exception("Command '$cmd' exited with code: ${process.exitValue()}")
        }
    }

    void maven(List<String> cmd) {
        def exeExtension = System.properties.'os.name'.toLowerCase().contains('windows') ? '.cmd' : ''
        String mvnExecutable = "${System.properties.'maven.home'}${File.separator}bin${File.separator}mvn${exeExtension}"
        String[] args = [mvnExecutable] + (cmd as List);
        run(args)
    }

    void npx(String cmd) {
        maven(["-N", "frontend:npx", "-Dfrontend.npx.arguments=${cmd}"])
    }

    void npm(String cmd) {
        maven(["-N", "frontend:npm", "-Dfrontend.npm.arguments=${cmd}"])
    }
}

ObjectMapper jsonObjectMapper = new ObjectMapper()

File basedir = new File(new File(request.outputDirectory), request.artifactId)
File frontendDir = new File(basedir, 'frontend')
File frontendGenerateDir = new File(basedir, request.artifactId)
File frontendTempDir = new File(basedir, 'frontend-temp')

Project baseProject = new Project(projectDirectory: basedir)
Project frontendProject = new Project(projectDirectory: frontendDir)

//Install node/npm for base project/bootstrap
baseProject.maven(["-N", "frontend:install-node-and-npm"])

//create-react-app and copy extra files in
Files.move(frontendDir.toPath(), frontendTempDir.toPath())
baseProject.npx "create-react-app ${request.artifactId} --template typescript --use-npm"
Files.move(frontendGenerateDir.toPath(), frontendDir.toPath())
FileUtils.copyDirectoryStructure(frontendTempDir, frontendDir)

//Set up frontend plugin for the frontend project now
frontendProject.maven(["-N", "frontend:install-node-and-npm"])

File frontendSrcDir = new File(frontendDir, "src")
File frontendPublicDir = new File(frontendDir, "public")

//Polyfills for IE11 (and HTMLUnit) support
frontendProject.npm "i --save react-app-polyfill"
File indexTsxFile = new File(frontendSrcDir, "index.tsx")
indexTsxFile.text = "import 'react-app-polyfill/ie11';\nimport 'react-app-polyfill/stable';\n" + indexTsxFile.text

//React router support

//Add dependency
frontendProject.npm "i --save react-router-dom"
frontendProject.npm "i --save-dev @types/react-router-dom"

//Update index.html with <base> tag after <meta> tag
File indexHtmlFile = new File(frontendPublicDir, "index.html")
indexHtmlFile.text = indexHtmlFile.text.replaceFirst(/(?i)<meta\s+[^>]+>/, '$0\n    <base href="/" />')

//Upgrade all dependencies to latest versions
frontendProject.npx "npm-check-updates -u"
frontendProject.npm "install"

//Storybook support
frontendProject.npx "-p @storybook/cli sb init"
File storybookMainJsFile = new File(frontendDir, ".storybook${File.separator}main.js")
def storybookMainJs = storybookMainJsFile.text.replace('*.stories.js', '*.stories.tsx')
storybookMainJsFile.text = storybookMainJs
frontendSrcDir.traverse(type: FileType.FILES, nameFilter : ~/.*\.stories\.js/) {
    Files.move(it.toPath(), new File(it.parentFile, it.name.substring(0, it.name.lastIndexOf('.stories.js')) + '.stories.tsx').toPath())
}

//ESLint
frontendProject.npm "i --save-dev eslint-plugin-react @typescript-eslint/eslint-plugin @typescript-eslint/parser"

//Update tsconfig.json
File tsConfigFile = new File(frontendDir, 'tsconfig.json')
def tsConfigJson = jsonObjectMapper.readValue(tsConfigFile, LinkedHashMap.class)
tsConfigJson.compilerOptions.experimentalDecorators = true
tsConfigJson.compilerOptions.emitDecoratorMetadata = true
jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValue(tsConfigFile, tsConfigJson)

//Update package.json
File packageJsonFile = new File(frontendDir, 'package.json')
def packageJson = jsonObjectMapper.readValue(packageJsonFile, LinkedHashMap.class)
packageJson.proxy = "http://localhost:8080"
packageJson.homepage = "."
packageJson.scripts.lint = "eslint . --ext .js,.jsx,.ts,.tsx"
packageJson.browserslist.development = packageJson.browserslist.production
jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValue(packageJsonFile, packageJson)

//Tidy up generated storybook examples for linting
File storybookSamplesDir = new File(frontendSrcDir, "stories")
storybookSamplesDir.traverse(type: FileType.FILES, nameFilter : ~/.*\.tsx/) {
    it.text = it.text.replace('user?: {};', 'user?: unknown;')
}

//Clean up
new File(frontendDir, ".git").deleteDir()
new File(frontendDir, ".gitignore").delete()
frontendTempDir.deleteDir()
