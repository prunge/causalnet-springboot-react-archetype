package ${package}.db;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

public interface MessageOfTheDayRepository extends CrudRepository<MessageOfTheDay, Long>, QuerydslPredicateExecutor<MessageOfTheDay>
{
}
