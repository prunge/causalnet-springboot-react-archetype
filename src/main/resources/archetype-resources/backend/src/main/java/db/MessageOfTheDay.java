package ${package}.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@Entity
public class MessageOfTheDay
{
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 1024)
    private String message;

    @NotNull
    private ZonedDateTime created;

    protected MessageOfTheDay()
    {
    }

    public MessageOfTheDay(String message, ZonedDateTime created)
    {
        this.message = message;
        this.created = created;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public ZonedDateTime getCreated()
    {
        return created;
    }

    public void setCreated(ZonedDateTime created)
    {
        this.created = created;
    }
}
