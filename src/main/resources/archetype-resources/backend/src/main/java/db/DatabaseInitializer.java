package ${package}.db;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

/**
 * Initializes the database for a first-time run if it is empty.
 */
@Component
public class DatabaseInitializer
{
    private final MessageOfTheDayRepository motdRepository;

    public DatabaseInitializer(MessageOfTheDayRepository motdRepository)
    {
        this.motdRepository = motdRepository;
    }

    /**
     * Called when Spring is initialized after beans have been set up.
     */
    @EventListener
    public void createMotdIfNoneExist(ContextRefreshedEvent event)
    {
        if (motdRepository.count() < 1L)
            motdRepository.save(new MessageOfTheDay("Good morning, what will be for eating?", ZonedDateTime.now()));
    }
}
