package ${package};

import au.net.causal.shoelaces.apphome.ApplicationHome;
import ${package}.db.MessageOfTheDay;
import ${package}.db.MessageOfTheDayRepository;
import com.google.common.collect.Iterators;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Component
@Path("/")
@SecurityScheme(name = "httpBasic", type = SecuritySchemeType.HTTP, scheme = "basic")
public class HelloResource
{
    private static final Logger log = LoggerFactory.getLogger(HelloResource.class);

    private final ApplicationHome appHome;
    private final MessageOfTheDayRepository motdRepository;

    public HelloResource(ApplicationHome appHome, MessageOfTheDayRepository motdRepository)
    {
        this.appHome = appHome;
        this.motdRepository = motdRepository;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("public/motd")
    public MessageOfTheDay motd()
    {
        return Iterators.getOnlyElement(motdRepository.findAll().iterator());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("hello")
    @SecurityRequirement(name = "httpBasic")
    public Greeting hello(@QueryParam("name") String name, @Context HttpServletRequest request)
    {
        log.info("Application home: " + appHome.getApplicationHomeDirectory());
        log.info("Request IP: " + request.getRemoteAddr());
        return new Greeting("Hello " + name);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("public/hello")
    public Greeting helloPublic(@QueryParam("name") String name)
    {
        return new Greeting("Welcome " + name);
    }

    public static class Greeting
    {
        private final String text;

        public Greeting(String text)
        {
            this.text = text;
        }

        public String getText()
        {
            return text;
        }
    }
}
