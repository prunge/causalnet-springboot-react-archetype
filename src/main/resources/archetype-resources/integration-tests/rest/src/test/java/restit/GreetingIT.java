package ${package}.restit;

import au.net.causal.shoelaces.testing.ServerEndpointExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.api.DefaultApi;
import org.openapitools.client.model.Greeting;
import org.openapitools.client.model.MessageOfTheDay;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.http.HttpClient;
import java.time.OffsetDateTime;

import static org.assertj.core.api.Assertions.*;

class GreetingIT
{
    @RegisterExtension
    static ServerEndpointExtension server = new ServerEndpointExtension()
            .usePath("/api");

    private DefaultApi api;

    @BeforeEach
    private void setUpApiClient()
    {
        ApiClient apiClient = new ApiClient();
        apiClient.setScheme(server.getUri().getScheme());
        apiClient.setHost(server.getHost());
        apiClient.setPort(server.getPort());
        apiClient.setBasePath(server.getUri().getRawPath());

        //Credential setup
        apiClient.setHttpClientBuilder(HttpClient.newBuilder().authenticator(new Authenticator()
        {
            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication("admin", "admin".toCharArray());
            }
        }));

        api = new DefaultApi(apiClient);
    }

    @Test
    void testMotd()
    throws ApiException
    {
        MessageOfTheDay result = api.motd();
        assertThat(result.getMessage()).isEqualTo("Good morning, what will be for eating?");
        assertThat(result.getCreated()).isBeforeOrEqualTo(OffsetDateTime.now());
    }

    @Test
    void testHelloWithCredentials()
    throws ApiException
    {
        Greeting result = api.hello("John Galah");
        assertThat(result.getText()).isEqualTo("Hello John Galah");
    }
}
