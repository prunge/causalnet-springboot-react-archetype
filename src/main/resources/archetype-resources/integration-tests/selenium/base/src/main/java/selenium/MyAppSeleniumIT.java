package ${package}.seleniumit;

import au.net.causal.shoelaces.testing.selenium.WebDriverTools;
import au.net.causal.shoelaces.testing.selenium.react.BaseReactFluentleniumTestCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.NoAlertPresentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.*;
import static org.fluentlenium.assertj.FluentLeniumAssertions.*;
import static org.fluentlenium.core.filter.FilterConstructor.*;

class MyAppSeleniumIT extends BaseReactFluentleniumTestCase
{
    private static final Logger log = LoggerFactory.getLogger(MyAppSeleniumIT.class);

    /**
     * Go to the application start page and wait for it to be displayed before each test.
     */
    @BeforeEach
    private void startUpOnMainPage()
    {
        goToMainPage();
    }

    /**
     * Just logs the browser version so we know exactly what we are testing with in the other tests.
     */
    @Test
    void reportBrowserDetails()
    {
        Capabilities caps = WebDriverTools.unwrapDriver(getDriver(), HasCapabilities.class).getCapabilities();
        log.info("Browser: " + caps.getBrowserName() + " " + caps.getVersion());
    }

    @Test
    void messageOfTheDayDisplays()
    {
        await().until($("div", containingText("Message of the day: Good morning, what will be for eating?"))).present();
        assertThat($("div", containingText("Message of the day:"))).hasText("Good morning, what will be for eating?");
    }

    @Test
    void openApiRequestButtonDisplaysMessage()
    {
        $("button", withText("OpenAPI Request!")).single().click();
        await().until(this::alertIsDisplayed);
        assertThat(alert().getText()).isEqualTo("Hello Jennifer Cockatoo");
        alert().dismiss();
    }

    private boolean alertIsDisplayed()
    {
        try
        {
            return alert().present();
        }
        catch (NoAlertPresentException e)
        {
            return false;
        }
    }
}
