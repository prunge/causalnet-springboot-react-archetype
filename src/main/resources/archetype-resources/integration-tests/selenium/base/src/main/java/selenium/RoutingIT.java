package ${package}.seleniumit;

import au.net.causal.shoelaces.testing.selenium.react.BaseReactFluentleniumTestCase;
import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.jupiter.api.Test;

import static org.fluentlenium.assertj.FluentLeniumAssertions.*;

class RoutingIT extends BaseReactFluentleniumTestCase
{
    @Test
    void mainPageHasExpectedContent()
    {
        goToMainPage();
        assertThat($("img")).hasClass("App-logo");
        assertThat($("a.App-link")).hasText("Learn React");
        assertThat($("a.App-link").single().attribute("href")).startsWith("https://reactjs.org");
    }

    @Test
    void mainPageHasNavBar()
    {
        goToMainPage();
        FluentList<FluentWebElement> navItems = $("nav li a");
        assertThat(navItems).hasSize(2);
        assertThat(navItems.get(0)).hasText("Home")
                                   .hasAttributeValue("href", server.getUri().toString())
                                   .isClickable();
        assertThat(navItems.get(1)).hasText("Another Page")
                                   .hasAttributeValue("href", server.getUri().resolve("another").toString())
                                   .isClickable();
    }

    @Test
    void navigationToSecondaryPageByJavascriptNavigation()
    {
        goToMainPage();

        //Check original page is displayed
        assertThat($("a.App-link")).isNotEmpty();

        //Use nav-bar to navigate to secondary page using React router
        FluentWebElement secondaryPageLink = $("nav li a").get(1);
        assertThat(secondaryPageLink).isClickable();
        secondaryPageLink.click(); //Click on 'Another Page' link
        FluentWebElement messageElement = $("div.anotherMessage").single();
        await().until(messageElement).displayed();

        //Check proper content of secondary page
        assertThat(messageElement).hasText("This is another page");

        //Check original page has gone away
        assertThat($("a.App-link")).isEmpty();
    }

    /**
     * Go directly to the secondary page /another by using a URL in the browser (like with a bookmark).
     * Tests that the server-side redirects work.
     */
    @Test
    void secondaryPageByDirectBrowserUrl()
    {
        goTo(server.getUri().resolve("another"));

        //Check proper content of secondary page
        assertThat($("div.anotherMessage").single()).hasText("This is another page");

        //Check we are not on the original page with the React logo
        assertThat($("a.App-link")).isEmpty();
    }
}
