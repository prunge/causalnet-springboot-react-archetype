module.exports = {
    parser: "@typescript-eslint/parser", // Specifies the ESLint parser
    parserOptions: {
        ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
        sourceType: "module", // Allows for the use of imports
        ecmaFeatures: {
            jsx: true // Allows for the parsing of JSX
        }
    },
    settings: {
        react: {
            version: "detect" // Tells eslint-plugin-react to automatically detect the version of React to use
        }
    },
    extends: [
        "plugin:react/recommended", // Uses the recommended rules from @eslint-plugin-react
        "plugin:@typescript-eslint/recommended" // Uses the recommended rules from @typescript-eslint/eslint-plugin
    ],
    rules: {
        // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
        "@typescript-eslint/explicit-function-return-type": ["warn", {
            "allowExpressions": true
        }],
        "react/no-unescaped-entities": ["off"],
    },
    overrides: [
        {
            // Relax return types for storybook
            "files": ["*.stories.tsx"],
            "rules": {
                "@typescript-eslint/explicit-function-return-type": ["off"]
            }
        },
        {
            // Special case for storybook supporting files
            "files": ["**/stories/*.tsx"],
            "rules": {
                "react/prop-types": ["error", {"skipUndeclared": true}],

            }
        }
    ]
};
