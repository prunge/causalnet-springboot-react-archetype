/**
 * Returns the document base URI, with an appropriate fallback for older browsers that do not support it.
 */
export function documentBaseUri(): string {
    let uri = document.baseURI;
    if (uri) {
        return uri;
    }

    // Fallback for IE11
    const bases = document.getElementsByTagName('base');
    for (let i = 0; i < bases.length; i++) {
        uri = bases[i].href;
        if (uri) {
            return uri;
        }
    }

    // Fallback if no base element
    return document.URL;
}
