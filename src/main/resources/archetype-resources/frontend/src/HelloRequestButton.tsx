import * as React from 'react';
import {documentBaseUri} from "./document-base-uri";

export interface HelloRequestButtonProps {
    name: string;
}

/**
 * A simple button that makes a REST request to the server and displays response in an alert.
 */
export class HelloRequestButton extends React.Component<HelloRequestButtonProps> {
    render(): JSX.Element {
        return (
            <div>
                <button onClick={async () => {
                    try {
                        const response = await fetch(`${documentBaseUri()}api/public/hello?name=${this.props.name}`)
                        const responseObj = await response.json();
                        alert(responseObj.text);
                    } catch (err) {
                        alert(`Request failed: ${err instanceof Error ? err.message : err}`)
                    }
                    }}>Request!
                </button>
            </div>
        );
    }
}
