import React from 'react';
import {BrowserRouter, NavLink, Route, Routes} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import {documentBaseUri} from "./document-base-uri";
import {HelloRequestButton} from "./HelloRequestButton";
import {HelloRequestOpenApiButton} from "./HelloRequestOpenApiButton";
import {MessageOfTheDay} from "./MessageOfTheDay";

export default function App(): JSX.Element {
  return (
    <BrowserRouter basename={new URL(documentBaseUri()).pathname}>
      <nav>
        <ul>
          <li>
            <NavLink to="/" end style={({ isActive }) => (isActive ? ({color: "red"}) : ({}))}>Home</NavLink>
          </li>
          <li>
            <NavLink to="/another" end style={({ isActive }) => (isActive ? ({color: "red"}) : ({}))}>Another Page</NavLink>
          </li>
        </ul>
      </nav>

      <Routes>
        <Route path="/another" element={<div className="anotherMessage">This is another page</div>} />
        <Route path="/" element={
          <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <MessageOfTheDay />
              <p>
                Edit <code>src/App.tsx</code> and save to reload.
              </p>
              <a
                  className="App-link"
                  href="https://reactjs.org"
                  target="_blank"
                  rel="noopener noreferrer"
              >
                Learn React
              </a>
              <HelloRequestButton name="John Galah" />
              <HelloRequestOpenApiButton name="Jennifer Cockatoo" />
            </header>
          </div>
        } />
      </Routes>
    </BrowserRouter>
  );
}
