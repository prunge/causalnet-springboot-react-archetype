import * as React from 'react';
import {DefaultApi} from "./generated/openapi/apis";
import {Configuration} from "./generated/openapi";
import {documentBaseUri} from './document-base-uri';

export interface HelloRequestButtonProps {
    name: string;
}

export class HelloRequestOpenApiButton extends React.Component<HelloRequestButtonProps> {
    render(): JSX.Element {
        const api = new DefaultApi(new Configuration({basePath: new URL('api', documentBaseUri()).pathname, username: 'admin', password: 'admin'}))
        return (
            <div>
                <button onClick={async () => {
                    const result = await api.hello({name: this.props.name});
                    alert(result.text);
                    }}>OpenAPI Request!
                </button>
            </div>
        );
    }
}
