import * as React from 'react';
import {Configuration} from "./generated/openapi";
import {DefaultApi} from "./generated/openapi/apis";

interface MessageOfTheDayState {
    message?: string;
}

export class MessageOfTheDay extends React.Component<unknown, MessageOfTheDayState> {
    constructor(props: unknown) {
        super(props);
        this.state = {};
    }

    async componentDidMount(): Promise<void> {
        const api = new DefaultApi(new Configuration({basePath: '/api'}));
        const motd = await api.motd();
        this.setState({message: motd.message});
    }

    render(): JSX.Element {
        return (
            <div>
                Message of the day: {this.state.message ?? '...'}
            </div>
        );
    }
}
